#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2022 Pavel Ditenbir (pavel.ditenbir@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import pickle
import pprint
import re

import PIL
import face_recognition
import numpy as np
from PIL import Image

import config
import os
import sys

from ctypes import *
from typing import List

FACE_DATASET_FILENAME = 'face_dataset.dat'

KNOWN_NAMES = []
KNOWN_FACE_ENCODINGS = []
OUTPUT_SET = dict()


def parse_cmdline() -> (argparse.ArgumentParser, List):
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', metavar='file', required=True, help='file to process')
    parser.add_argument('--debug', default=None, action='store_true', dest="debug_log", help='print debug information')

    parsed_args = parser.parse_args()

    return parser, parsed_args


def detect_faces(frame_num, frame):
    import face_recognition

    img = frame
    im = Image.fromarray(img)
    im.save(os.path.join('output', f'frame_{frame_num}.jpeg'))

    face_locations = face_recognition.face_locations(frame, number_of_times_to_upsample=0)
    for idx, face_location in enumerate(face_locations):
        top, right, bottom, left = face_location
        print(f"    ({left}, {top}, {right}, {bottom})")

        MARGIN = 15

        filename = os.path.join('output', f'frame_{frame_num}_{idx}.jpeg')
        cropped_img = im.crop((left - MARGIN, top - MARGIN, right + MARGIN, bottom + MARGIN))
        cropped_img.save(filename)

        test_image(np.array(cropped_img), KNOWN_NAMES, KNOWN_FACE_ENCODINGS)


def process_file(filename: str):
    import cv2

    video_capture = cv2.VideoCapture(filename)
    frame_count = 0

    while video_capture.isOpened():
        ret, frame = video_capture.read()

        if not ret:
            break

        print(f'#{frame_count}')
        detect_faces(frame_count, frame)

        frame_count += 1


def test_image(image_to_check, known_names, known_face_encodings, tolerance=0.5, show_distance=False):
    # image_to_check = face_recognition.load_image_file(image_to_check_filename)

    unknown_encodings = face_recognition.face_encodings(image_to_check)

    def print_result(name, distance):
        if name != 'unknown_person':
            global OUTPUT_SET
            OUTPUT_SET[name] = distance
        else:
            # TODO add to notice list for later adding
            pass

    for unknown_encoding in unknown_encodings:
        distances = face_recognition.face_distance(known_face_encodings, unknown_encoding)
        result = list(distances <= tolerance)

        if True in result:
            [print_result(name, distance) for is_match, name, distance in
             zip(result, known_names, distances) if is_match]
        else:
            # TODO add to notice list for later adding
            pass


def process_file_separate_cli(filename: str, lib_path: str, lib_filename: str):
    import numpy as np

    lib = CDLL(lib_filename)

    # int StartFileProcessing(const char *filename, OnFrame onFrame,
    #                   OnFinished onFinished, const char *licenseKey);

    # typedef int (*OnFrame)(uint64_t frameNum, const unsigned char *data, int size);
    # typedef int (*OnFinished)(int errCode);

    def on_frame(frame_num, data, size):
        from PIL import Image

        width = 1280
        height = 720
        components = 3

        np_arr = np.ctypeslib.as_array(
            (c_uint8 * size).from_address(addressof(data.contents)))

        np_buf = np.frombuffer(np_arr, np.uint8)
        img = np_buf.reshape(height, width, components)
        detect_faces(frame_num, img)

        return 0

    def on_finished(err_code):
        print(f'on_finished({err_code})')
        return 0

    ON_FRAME_FUNC = CFUNCTYPE(c_int, c_uint64, POINTER(c_uint8), c_int)
    on_frame_callback = ON_FRAME_FUNC(on_frame)

    ON_FINISHED_FUNC = CFUNCTYPE(c_int, c_int)
    on_finished_callback = ON_FINISHED_FUNC(on_finished)

    start_file_processing = lib.ProcessBuf
    start_file_processing.restype = c_int
    start_file_processing.argtypes = [POINTER(c_uint8), c_int, ON_FRAME_FUNC, ON_FINISHED_FUNC, POINTER(c_uint8)]

    lib.StartFileProcessing(c_char_p(filename.encode('utf-8')), on_frame_callback, on_finished_callback,
                            c_char_p(os.environ['ORIS_FH_CLI_FRAME_PROVIDER_LICENSE_KEY'].encode('utf-8')))


def image_files_in_folder(folder):
    out = list()
    for d in os.listdir(folder):
        for f in os.listdir(os.path.join(folder, d)):
            if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I):
                out.append(os.path.join(os.path.join(folder, d), f))

    return out


def scan_known_people(known_people_folder):
    global KNOWN_NAMES
    global KNOWN_FACE_ENCODINGS

    if os.path.exists(FACE_DATASET_FILENAME):
        with open(FACE_DATASET_FILENAME, 'rb') as f:
            data = pickle.load(f)
            KNOWN_NAMES = data['known_names']
            KNOWN_FACE_ENCODINGS = data['known_face_encodings']
    else:
        for file in image_files_in_folder(known_people_folder):
            basename = os.path.splitext(os.path.basename(file))[0]
            img = face_recognition.load_image_file(file)
            encodings = face_recognition.face_encodings(img)

            if len(encodings) > 1:
                print("WARNING: More than one face found in {}. Only considering the first face.".format(file))

            if len(encodings) == 0:
                print("WARNING: No faces found in {}. Ignoring file.".format(file))
            else:
                KNOWN_NAMES.append(basename)
                KNOWN_FACE_ENCODINGS.append(encodings[0])

        with open(FACE_DATASET_FILENAME, 'w+b') as f:
            pickle.dump({
                'known_names': KNOWN_NAMES,
                'known_face_encodings': KNOWN_FACE_ENCODINGS
            }, f)


if __name__ == '__main__':
    cmdline_parser, args = parse_cmdline()

    scan_known_people('/Users/dipaolo/Downloads/lfw')

    if 'debug_log' in args and args.debug_log:
        config.DEBUG_LOG = args.debug_log

    input_filename = ''
    if 'input' in args and args.input:
        input_filename = args.input

    # process_file(input_filename)
    process_file_separate_cli(input_filename,
                              os.environ['ORIS_FH_CLI_FRAME_PROVIDER_LIB_PATH'],
                              os.environ['ORIS_FH_CLI_FRAME_PROVIDER_LIB_NAME'])

    pprint.pprint(OUTPUT_SET)

    sys.exit(os.EX_OK)
