#!/usr/bin/env bash

# input parameters:
#   $1 - job number
#   $2 - update parameters

rm -rf docker-compose.yml update.sh
curl -L https://gitlab.com/dipaolo/oris/-/jobs/$1/artifacts/download --output artifact.zip
unzip artifact.zip
rm -rf artifact.zip

./update.sh $2
