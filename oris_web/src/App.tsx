import React from 'react';
import './App.css';
import {Container, MantineProvider, SimpleGrid, Skeleton, useMantineTheme} from '@mantine/core';
import {BrowserRouter as Router, Navigate, Route, Routes, Outlet} from "react-router-dom";
import useToken from "./components/App/useToken";
import {AppHeader} from "./components/App/AppHeader";
import Login from './components/Login/Login';
import ServersPage from './components/Servers/ServersPage';
import JobsPage from './components/Jobs/JobsPage';

const getChild = (height: number) => <Skeleton height={height} radius="md" animate={false}/>;
const BASE_HEIGHT = 360;
const getSubHeight = (children: number, spacing: number) =>
    BASE_HEIGHT / children - spacing * ((children - 1) / children);


function App() {
    const theme = useMantineTheme();
    const {token, setToken, clearToken} = useToken();

    return (
        <MantineProvider withGlobalStyles withNormalizeCSS>
            <Router>
                {token != null ?
                    <Container my="md">
                        <AppHeader links={[
                            {link: '/jobs', label: 'Jobs'},
                            {link: '/servers', label: 'Servers'},
                            {link: '/about', label: 'About'},
                        ]}/>
                        <SimpleGrid cols={2}>
                            <Outlet/>
                        </SimpleGrid>
                    </Container>
                    :
                    <Login setToken={setToken}/>
                }

                <Routes>
                    <Route path="/" element={token != null ? <Home/> : <Navigate to="/login" replace/>}/>
                    <Route path="/jobs" element={<JobsPage/>}/>
                    <Route path="/servers" element={<ServersPage/>}/>
                    <Route path="/about" element={<About/>}/>
                </Routes>
            </Router>
        </MantineProvider>

    );
}

export default App;

function Home() {
    return <h2>Home</h2>;
}

function About() {
    return <h2>About</h2>;
}