import { useForm } from '@mantine/form';
import {PasswordInput, Group, Button, Box, TextInput} from '@mantine/core';
// import {useHistory} from "react-router-dom";
import {useState} from "react";
import {useNavigate} from "react-router-dom";

async function loginUser(login: string | undefined, password: string | undefined) {
    console.log("Logging in as " + login + ":" + password)
    return fetch('' +
        '/api/users/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "login": login,
            "password": password,
        })
    })
        .then(data => data.json())
        .catch((err) => {
            console.log(err)
        })
}

// @ts-ignore
export default function Login({setToken}) {
    // const navigate = useNavigate();

    // const [username, setUserName] = useState();
    // const [password, setPassword] = useState();

    const handleSubmit = async () => {
        // e: { preventDefault: () => void; }) => {
        // e.preventDefault();
        const token = await loginUser(form.values.login, form.values.password);
        console.log('====');
        console.log(form.values);
        console.log(form.values.login);
        setToken(token);
        // navigate("/");
    }

    const form = useForm({
        initialValues: {
            login: '',
            password: '',
        },

        validate: {
            // confirmPassword: (value, values) =>
            //     value !== values.password ? 'Passwords did not match' : null,
        },
    });

    return (
        // <Box sx={{ maxWidth: 340 }} mx="auto">
        //     <form onSubmit={form.onSubmit((values) => console.log(values))}>
        //         <TextInput label="Login" placeholder="user" {...form.getInputProps('name')} />
        //
        //         <PasswordInput
        //             label="Password"
        //             placeholder="p@s$w0rd"
        //             {...form.getInputProps('password')}
        //         />
        //
        //         <Group position="center" mt="md">
        //             <Button type="submit">Login</Button>
        //         </Group>
        //     </form>
        // </Box>

    <div style={{ maxWidth: 320, margin: 'auto', marginBlock: '160px' }}>
        <TextInput label="Login" placeholder="user" style={{ marginBlock: '16px' }} {...form.getInputProps('login')} />
        <PasswordInput label="Password" placeholder="p@s$w0rd" style={{ marginBlock: '16px' }} {...form.getInputProps('password')} />

        <Group position="center" mt="xl">
            <Button
                variant="outline"
                style={{ marginBlock: '6px' }}
                onClick={() =>
                    handleSubmit()

                    // form.setValues({
                    //     name: ,
                    //     email: `${randomId()}@test.com`,
                    // })
                }
            >
                Login
            </Button>
        </Group>
    </div>

);
}
