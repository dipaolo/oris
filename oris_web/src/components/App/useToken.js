import { useState } from 'react';

export default function useToken() {
    const getToken = () => {
        const tokenString = sessionStorage.getItem('token');
        if (tokenString == null || tokenString === "undefined") {
            return null;
        }

        const token = JSON.parse(tokenString);
        return token?.token;
    };

    const [token, setToken] = useState(getToken());

    const saveToken = token => {
        console.log("token", token);
        if (token != null) {
            sessionStorage.setItem('token', JSON.stringify(token));
            setToken(token.token);
        }
    };

    const clearToken = () => {
        sessionStorage.removeItem('token');
        setToken(null);
    };

    return {
        setToken: saveToken,
        token,
        clearToken,
    }
}
