# MIT License
#
# Copyright (c) 2022 Pavel Ditenbir (pavel.ditenbir@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import config
import m3u8
import textwrap
import time

from typing import Dict, List, Callable, Optional
from utils import does_url_exist, get_url_path_without_filename, join_url


def start_downloading_stream(url: str, stream_duration_sec: int, on_segment: Callable[[str], None]) -> int:
    """Will process the only one layer (with the highest bitrate)"""

    # first, check the URL is valid and exists; return an error otherwise
    if not does_url_exist(url):
        print('ERROR: invalid URL')
        return 1

    # choose rendition with the maximum bitrate
    target_bandwidth = max(get_bandwidths(url))

    segment_duration = get_segment_duration(url)
    last_downloaded_time = 0
    appeared_segments = list()
    sent_segments = list()
    total_downloaded_duration_sec = 0

    # the idea is:
    # 1. get the list of current segments
    # 2. while we have time left until the new segment appears, send segments
    #    that haven't been sent yet (diff between appeared_segments and sent_segments)
    # 3. sleep for the expected time left until next segment to be appeared
    #
    # IMPORTANT: that order doesn't correspond to the order of lines of code below
    while total_downloaded_duration_sec < stream_duration_sec:
        def get_time_left():
            """time left until next segment"""
            cur_time = time.perf_counter()
            diff = cur_time - last_downloaded_time
            return segment_duration - diff

        def get_items_to_send() -> List[str]:
            out = list()
            for appeared_segm in appeared_segments:
                if appeared_segm not in sent_segments:
                    out.append(appeared_segm)
            return out

        while True:
            # send at least one, if any
            items_to_send = get_items_to_send()
            if len(items_to_send) > 1:
                item_to_send = items_to_send[0]
                if on_segment:
                    on_segment(item_to_send)
                sent_segments.append(item_to_send)
            else:
                break

            if get_time_left() <= 0:
                break

        time_left = get_time_left()
        if time_left > 0:
            time.sleep(time_left)

        cur_segments = get_segments_urls(url, target_bandwidth)[target_bandwidth]
        # IMPORTANT save time before all other calculations
        last_downloaded_time = time.perf_counter()

        # we must keep the order of segments, so can't use set (which could be less complicated solution)
        # so, we will make some magic with lists:
        #   - remove outdated segments (the ones which aren't in the media playlist anymore)
        #   - add the new ones (which appeared in media playlist, but missed in appeared_segments)

        # remove outdated segments from appeared_segments

        items_to_remove = list()
        for appeared_segm in appeared_segments:
            if appeared_segm not in url:
                items_to_remove.append(appeared_segm)

        for item in items_to_remove:
            appeared_segments.remove(item)

        # add new ones
        for cur_segm in cur_segments:
            if cur_segm not in appeared_segments:
                appeared_segments.append(cur_segm)

    return 0


def get_segments_urls(url: str, rendition: Optional[int] = None) -> Dict[int, List[str]]:
    """
    Get the list of URLs for specified rendition, or for all renditions
    :param url: HLS address
    :param rendition: rendition; the rendition's bitrate is used as the index
    :return: the list of URLs for specified renditions, or for all renditions in case rendition is None
    """

    master_playlist = m3u8.load(url)
    if config.DEBUG_LOG:
        print(master_playlist.dumps())

    if not master_playlist.is_variant:
        return dict()

    indentation = '  '
    out = dict()
    for media_playlist_desc in master_playlist.playlists:
        bandwidth = media_playlist_desc.stream_info.bandwidth
        is_target_rendition = rendition is None or bandwidth == rendition

        if not is_target_rendition:
            continue

        if config.DEBUG_LOG:
            print(textwrap.indent(f'URI: {media_playlist_desc.uri}', 1 * indentation))
            print(textwrap.indent(f'Bandwidth: {bandwidth}', 1 * indentation))
            print(f'')

        base_path = get_url_path_without_filename(url)
        media_url = join_url(base_path, media_playlist_desc.uri)
        media_playlist = m3u8.load(media_url)

        if config.DEBUG_LOG:
            print(textwrap.indent(media_playlist.dumps(), 2 * indentation))
            print(f'')

        out[bandwidth] = [join_url(media_playlist.base_uri, s.uri) for s in media_playlist.segments]

    return out


def get_segment_duration(url: str) -> Optional[float]:
    master_playlist = m3u8.load(url)

    if not master_playlist.is_variant:
        return None

    durations = list()
    for media_playlist_desc in master_playlist.playlists:
        base_path = get_url_path_without_filename(url)
        media_url = join_url(base_path, media_playlist_desc.uri)
        media_playlist = m3u8.load(media_url)
        durations.append(media_playlist.data['targetduration'])

    return max(durations)


def get_bandwidths(url: str) -> List[int]:
    master_playlist = m3u8.load(url)

    if not master_playlist.is_variant:
        return list()

    return [p.stream_info.bandwidth for p in master_playlist.playlists]
