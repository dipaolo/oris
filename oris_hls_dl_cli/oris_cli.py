#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2022 Pavel Ditenbir (pavel.ditenbir@gmail.com)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import config
import datetime
import os
import pprint
import sys

from hls import get_segments_urls, start_downloading_stream
from network import download_file
from typing import List


def parse_cmdline() -> (argparse.ArgumentParser, List):
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', metavar='URL', required=True, help='HLS stream')
    parser.add_argument('-d', '--dir', metavar='dir', default='.', help='output directory')
    parser.add_argument('--debug', default=None, action='store_true', dest="debug_log", help='print debug information')

    parsed_args = parser.parse_args()

    return parser, parsed_args


if __name__ == '__main__':
    cmdline_parser, args = parse_cmdline()

    if 'debug_log' in args and args.debug_log:
        config.DEBUG_LOG = args.debug_log

    input_url = ''
    if 'input' in args and args.input:
        input_url = args.input

    output_dir = ''
    if 'dir' in args and args.dir:
        output_dir = args.dir

    segments = get_segments_urls(input_url)
    if config.DEBUG_LOG:
        pprint.pprint(segments)

    def on_segment(url: str):
        print(f'{datetime.datetime.now()} ===> {url}')
        download_file(url, output_dir)

    start_downloading_stream(input_url, 10, on_segment)

    sys.exit(os.EX_OK)
